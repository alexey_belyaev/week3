<?php

/**
* Implements hook_views_data_alter().
*/
function mailfish_views_data_alter(&$data) {
  $data['mailfish_mails']['created_value']['filter']['handler'] = 'views_handler_filter_date';
  // print_r($data);
 return $data;
}


/**
 * Implements hook_views_data()
 */
function mailfish_views_data() {

  $table = array(
    'mailfish_mails' => array(  // Mydons Custom table
     'table' => array(
        'group' => t('Mailfish'),      // Groupname Mydons
        'base' => array(
          'sid' => 'subscribe id',  //Primary key
          'uid' => 'user id',
          'nid' => 'node id',
          'created' => 'created date',
         )
      ),

      //Description of sid
      'sid' => array(  
        'title' => t('Mailfish Custom Id'),
        'help' => t('Mailfish Custom table Id field'),
        // is mydons_custom_id field sortable TRUE
        'field' => array(
          'click sortable' => TRUE,  
        ),
        //Filter handler for filtering records by sid
        'filter' => array(
           'handler' => 'views_handler_filter_numeric'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        )
       ),

      //Description of uid field
      'uid' => array(
        'title' => t('User Id'),
        'help' => t('User Id'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort'
        ),
        'relationship'=> array(
          'base' => 'users',
          'base field' => 'uid',
          'handler' => 'views_handler_relationship',
          'label' => t('User ID relationship'),
          'title' => t('User ID relationship'),
          'help' => t('User ID relationship'),
        )
      ),

     //Description of nid field
      'nid' => array(
        'title' => t('Node id'),
        'help' => t('Node id'),
        'field' => array(
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_numeric'
        ),
        'sort' => array(
           'handler' => 'views_handler_sort'
        ),
        'relationship'=> array(
          'base' => 'node',
          'base field' => 'nid',
          'handler' => 'views_handler_relationship',
          'label' => t('Node ID relationship'),
          'title' => t('Node ID relationship'),
          'help' => t('Node ID relationship'),
        )
      ),

     //Description of date field
      'created' => array(
        'title' => t('Date'),
        'help' => t('Created date'),
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date'
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date'
        )
      ),
    )
  );
  return $table;
}
