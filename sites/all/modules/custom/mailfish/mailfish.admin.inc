<?php
// $Id$

/**
* Build and return the mailfish settings form
*/
function mailfish_admin_settings() {
  $form['mailfish_allowed_content_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allowed content types'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('mailfish_allowed_content_type', array()),
    '#description' => t('Chose content types that will be allowed to subscribe.'),
    '#required' => FALSE,
  );
  // debug($form);
  return system_settings_form($form);
  // return $form;
}
/*

function mailfish_admin_settings_submit($form, &$form_state) {
  debug($form_state);
}
*/